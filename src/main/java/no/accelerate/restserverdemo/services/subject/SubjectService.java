package no.accelerate.restserverdemo.services.subject;

import no.accelerate.restserverdemo.models.Subject;
import no.accelerate.restserverdemo.services.CrudService;

public interface SubjectService extends CrudService<Subject, Integer> {
}

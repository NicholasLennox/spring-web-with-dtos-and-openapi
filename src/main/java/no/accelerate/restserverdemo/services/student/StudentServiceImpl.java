package no.accelerate.restserverdemo.services.student;


import no.accelerate.restserverdemo.exceptions.StudentNotFoundException;
import no.accelerate.restserverdemo.models.Student;
import no.accelerate.restserverdemo.repositories.StudentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Implementation of the Student service.
 * Uses the Student repository to interact with the data store.
 * Logs errors through the standard logger.
 */
@Service
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;
    private final Logger logger = LoggerFactory.getLogger(StudentServiceImpl.class);

    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public Student findById(Integer id) {
        return studentRepository.findById(id)
                .orElseThrow(() -> new StudentNotFoundException(id));
    }

    @Override
    public Collection<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Student add(Student entity) {
        return studentRepository.save(entity);
    }

    @Override
    public Student update(Student entity) {
        return studentRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {
        studentRepository.deleteById(id);
    }

    @Override
    public boolean exists(Integer id) {
        return studentRepository.existsById(id);
    }

    @Override
    public Collection<Student> findAllByName(String name) {
        return studentRepository.findAllByName(name);
    }
}
